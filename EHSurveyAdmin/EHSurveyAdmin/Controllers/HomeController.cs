﻿using EHSurveyAdmin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using EHSurveyAdmin.Service;
using EHSurveyAdmin.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.Extensions.Configuration;

namespace EHSurveyAdmin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpContextAccessor _accessor;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor accessor, IConfiguration configuration)
        {
            _logger = logger;
            _accessor = accessor;
            _configuration = configuration;
        }
        /// <summary>
        /// Main page display for entering in the employee id for search
        /// </summary>
        /// <returns>Unauthorized if not in one of the approved AD Groups, or Search page if authorized</returns>
        public IActionResult Index()
        {
            var adService = new AdService(_accessor);

            if (adService.IsInRole("BHSQL Web Team Developers") || adService.IsInRole(_configuration.GetSection("EmployeeHealthAdGroup").Value))
            {
                return View();
            }
            return View("Unauthorized");
        }
        /// <summary>
        /// Submit action for Search by Employee ID
        /// </summary>
        /// <param name="employeeId">Employee ID from search box</param>
        /// <returns>Redirects to the Survey Home page</returns>
        [HttpPost]
        public IActionResult Index(string employeeId)
        {
            TempData["employeeId"] = employeeId;

            return RedirectToAction("Index", "Survey");
        }
        /// <summary>
        /// This is not really used. We could put a privacy page but not needed. 
        /// </summary>
        /// <returns></returns>
        public IActionResult Privacy()
        {
            return View();
        }
        /// <summary>
        /// Error page
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
    }
}
