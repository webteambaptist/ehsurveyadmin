﻿using System;
using EHSurveyAdmin.Interfaces;
using EHSurveyAdmin.Models;
using EHSurveyAdmin.Service;
using EHSurveyAdmin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EHSurveyAdmin.Controllers
{
    public class SurveyController : Controller, ILoggerInterface
    {
        private readonly string _userId;
        private readonly LoggerService _loggerService;
        private readonly EntityService _entityService;
        /// <summary>
        /// Constructor that takes in the dbContext and ContextAccessor from Startup
        /// </summary>
        /// <param name="context">dbConnection</param>
        /// <param name="httpContextAccessor">User Info about Logged in user</param>
        public SurveyController(EHSurveyContext context, IHttpContextAccessor httpContextAccessor)
        {
            // construct the services
            _loggerService = CreateLogger();
            var adService = new AdService(httpContextAccessor);
            _entityService = new EntityService(context);

            var user = adService.GetUserName();
            _userId = adService.CleanUsername(user);
        }
        /// <summary>
        /// This gets a list of surveys for a particular employee
        /// </summary>
        /// <param name="search">employee Id from Form</param>
        /// <returns>Model to View</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Index([FromForm] Search search)
        {
            try
            {
                var surveyModel = new SurveyModel();
                var employee = _entityService.GetEmployee(search.EmployeeId);
                var results = _entityService.GetEmployeeSurveyResults(search.EmployeeId);

                surveyModel.Employee = employee;
                surveyModel.Results = results;

                return View(surveyModel);
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: Index(Search) :: Search (EmployeeID): {search.EmployeeId}");
                var error = new ErrorViewModel
                {
                    Exception = e.Message
                };
                return View("Error", error);
            }
            
        }
        /// <summary>
        /// This is used for going back to list and passing employee id
        /// </summary>
        /// <param name="id">Employee ID</param>
        /// <returns>List of employee's surveys </returns>
        public IActionResult IndexReload(string id)
        {
            try
            {
                var surveyModel = new SurveyModel();
                var employee = _entityService.GetEmployee(id);
                var results = _entityService.GetEmployeeSurveyResults(id);

                surveyModel.Employee = employee;
                surveyModel.Results = results;

                return View("Index",surveyModel);
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: Index(id) :: Search (EmployeeID): {id}");
                var error = new ErrorViewModel
                {
                    Exception = e.Message
                };
                return View("Error", error);
            }
            
        }
        /// <summary>
        /// Gets details about a particular surven
        /// </summary>
        /// <param name="id">Survey ID</param>
        /// <param name="type">Type of survey (negative/positive)</param>
        /// <param name="employeeId">EmployeeID</param>
        /// <returns>Model to View</returns>
        [HttpGet]
        public IActionResult GetDetail(int id, string type, string employeeId)
        {
            try
            {
                var survey = new PositiveSurvey();
                var result = _entityService.GetSurvey(id);
                if (type == "positive")
                {
                    var positiveModel = new SurveyDetailsPositiveModel();
                    var model = JsonConvert.DeserializeObject<PositiveSurvey>(result.SurveyResultsJson);
                    model.SurveyDate = result.EntryDt;
                    positiveModel.PositiveSurvey = model;
                    positiveModel.Employee = _entityService.GetEmployee(employeeId);
                    return View("PositiveDetail", positiveModel);
                }
                else
                {
                    var negativeModel = new SurveyDetailsNegativeModel();
                    var model = (NegativeSurvey)JsonConvert.DeserializeObject<NegativeSurvey>(result.SurveyResultsJson);
                    model.SurveyDate = result.EntryDt;
                    negativeModel.NegativeSurvey = model;
                    negativeModel.Employee = _entityService.GetEmployee(employeeId);
                    return View("NegativeDetail", negativeModel);
                }
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: GetDetail :: ID: {id}, Type: {type}, EmployeeID: {employeeId} ");
                var error = new ErrorViewModel
                {
                    Exception = e.Message
                };
                return View("Error", error);
            }
           
        }
        /// <summary>
        /// Creates an instance of hte logger
        /// </summary>
        /// <returns>Logger service</returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("Survey");
            return logger;
        }
    }
}
