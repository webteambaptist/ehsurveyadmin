﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(function() {
    $.fn.dataTable.moment("MM/DD/YYYY hh:mm:ss A");
    // $.fn.dataTable.moment("MM-DD-YYYY hh:mm:ss A");
    $('#SurveyTable').DataTable({
        "dom": '<"top"i>rt<"bottom"lp><"clear">',
        "destroy": true,
        "language": {
            "emptyTable": "This employee does not have any Survey results."
        },
        "pageLength": 10,
        "pagingType": "full_numbers",
        "scrollY": "500px",
        responsive: true,
        "order": [[ 1, "desc" ]],
        columnDefs: [
            { targets: [1],  render: function (data, type, row) {//data
                return moment(row[1]).format('MM/DD/YYYY hh:mm:ss A');
            }}
        ]
    });
});
