﻿using System.Collections.Generic;
using EHSurveyAdmin.Models;

namespace EHSurveyAdmin.ViewModels
{
    public class SurveyModel
    {
        public Employee Employee { get; set; }
        public List<SurveyResult> Results { get; set; }
    }
}
