﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EHSurveyAdmin.ViewModels
{
    public class PositiveSurvey
    {
        [DisplayName("Survey Date")]
        public DateTime SurveyDate { get; set; }
        [DisplayName("Out of Work")]
        public bool OutOfWork { get; set; }
        [DisplayName("Had Covid-19 Symptoms")]
        public bool HadSymptoms { get; set; }
        [DisplayName("5 days since Covid-19 symptoms")]
        public bool FiveDaysSinceSymptoms { get; set; }
        [DisplayName("5 days since Positive Covid-19 test")]
        public bool FiveDaysSincePositiveTest { get; set; }
        [DisplayName("Fully Vaccinated")]
        public bool FullVaccinated { get; set; }
        [DisplayName("Chills")]
        public bool Chills { get; set; }
        [DisplayName("Cough")]
        public bool Cough { get; set; }
        [DisplayName("Shortness of Breath")]
        public bool ShortnessOfBreath { get; set; }
        [DisplayName("Fatigue")]
        public bool Fatigue { get; set; } 
        [DisplayName("Muscle Aches")]
        public bool MuscleAche { get; set; }
        [DisplayName("Headache")]
        public bool Headache { get; set; }
        [DisplayName("Sore Throat")]
        public bool SoreThroat { get; set; }
        [DisplayName("Congestion")]
        public bool Congestion { get; set; }
        [DisplayName("Runny Nose")]
        public bool RunnyNose { get; set; }
        [DisplayName("Nausea")]
        public bool Nausea { get; set; }
        [DisplayName("Vomiting")]
        public bool Vomiting { get; set; }
        [DisplayName("Diarrhea")]
        public bool Diarrhea { get; set; }
        [DisplayName("Fever")]
        public bool Fever { get; set; }
        [DisplayName("Taken Fever Meds")]
        public bool TakenFeverMeds { get; set; }
    }
}
