﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EHSurveyAdmin.ViewModels
{
    public class NegativeSurvey
    {
        [DisplayName("Survey Date")]
        public DateTime SurveyDate { get; set; }
        [DisplayName("Out of Work")]
        public bool OutOfWork { get; set; }
        [DisplayName("From PCR Test")]
        public bool FromPcrTest { get; set; }
        [DisplayName("Asymptomatic")]
        public bool Asymptomatic { get; set; }
        [DisplayName("24 Hours Since Symptoms")]
        public bool TwentyFourHoursSinceSymptoms { get; set; }
        [DisplayName("24 Hours Since Taking Meds")]
        public bool TwentyFourHoursSinceMeds { get; set; }
        [DisplayName("Not Exposed to Covid-19")]
        public bool NotExposedToCovid { get; set; }
        [DisplayName("Did Not Test Positive in Last 10 Days")]
        public bool NotTestPositiveLastTenDays { get; set; }
    }
}
