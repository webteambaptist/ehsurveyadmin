﻿using EHSurveyAdmin.Models;

namespace EHSurveyAdmin.ViewModels
{
    public class SurveyDetailsNegativeModel
    {
        public Employee Employee { get; set; }
        public NegativeSurvey NegativeSurvey { get; set; }
    }
}
