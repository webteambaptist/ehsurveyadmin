﻿using EHSurveyAdmin.Models;

namespace EHSurveyAdmin.ViewModels
{
    public class SurveyDetailsPositiveModel
    {
        public Employee Employee { get; set; }
        public PositiveSurvey PositiveSurvey { get; set; }
    }
}
