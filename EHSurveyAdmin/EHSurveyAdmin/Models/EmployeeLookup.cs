﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EHSurveyAdmin.Models
{
    public partial class EmployeeLookup
    {
        public string EmployeeId { get; set; }
        public string LevelOneManagerId { get; set; }
        public string LevelTwoManagerId { get; set; }
    }
}
