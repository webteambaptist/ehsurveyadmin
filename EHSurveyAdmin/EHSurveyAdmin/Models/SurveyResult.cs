﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EHSurveyAdmin.Models
{
    public partial class SurveyResult
    {
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string LevelOneManagerId { get; set; }
        public string LevelOneManagerName { get; set; }
        public string LevelTwoManagerId { get; set; }
        public string LevelTwoManagerName { get; set; }
        public string SurveyType { get; set; }
        public string SurveyResultsJson { get; set; }
        public DateTime EntryDt { get; set; }
    }
}
