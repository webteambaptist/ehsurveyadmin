﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EHSurveyAdmin.Models
{
    public partial class Employee
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Email { get; set; }
    }
}
