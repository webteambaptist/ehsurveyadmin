﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EHSurveyAdmin.Models
{
    public partial class EHSurveyContext : DbContext
    {
        public EHSurveyContext()
        {
        }

        public EHSurveyContext(DbContextOptions<EHSurveyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeLookup> EmployeeLookups { get; set; }
        public virtual DbSet<Manager> Managers { get; set; }
        public virtual DbSet<SurveyResult> SurveyResults { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.EmployeeId)
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EmployeeLookup>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("EmployeeLookup");

                entity.Property(e => e.EmployeeId)
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeID");

                entity.Property(e => e.LevelOneManagerId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("LevelOneManagerID");

                entity.Property(e => e.LevelTwoManagerId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("LevelTwoManagerID");
            });

            modelBuilder.Entity<Manager>(entity =>
            {
                entity.HasKey(e => new { e.EmployeeId, e.EmployeeName });

                entity.ToTable("Manager");

                entity.Property(e => e.EmployeeId)
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeName).HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SurveyResult>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.EntryDt).HasColumnType("datetime");

                entity.Property(e => e.LevelOneManagerId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("LevelOneManagerID");

                entity.Property(e => e.LevelOneManagerName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LevelTwoManagerId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("LevelTwoManagerID");

                entity.Property(e => e.LevelTwoManagerName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.SurveyResultsJson).IsRequired();

                entity.Property(e => e.SurveyType)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
