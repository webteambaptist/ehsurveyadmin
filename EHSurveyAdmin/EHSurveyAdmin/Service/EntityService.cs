﻿using System;
using System.Collections.Generic;
using System.Linq;
using EHSurveyAdmin.Interfaces;
using EHSurveyAdmin.Models;
using EHSurveyAdmin.ViewModels;

namespace EHSurveyAdmin.Service
{
    public class EntityService : ILoggerInterface
    {
        private readonly EHSurveyContext _context;
        private readonly LoggerService _loggerService;
        /// <summary>
        /// This is the constructor uses to create an instance of this object
        /// I takes in the dbContext so that a new instance does not need to be created.
        /// </summary>
        /// <param name="context">This is the dbContext created at startup</param>
        public EntityService(EHSurveyContext context)
        {
            _context = context;
            _loggerService = CreateLogger();
        }
        /// <summary>
        /// This creates a Logger instance
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("EntityService");
            return logger;
        }
        /// <summary>
        /// This gets details about a particular Survey 
        /// </summary>
        /// <param name="id">Survey ID from list</param>
        /// <returns></returns>
        public SurveyResult GetSurvey(int id)
        {
            try
            {
                var result = _context.SurveyResults.FirstOrDefault(x => x.Id == id);
                return result;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: GetPositiveSurvey :: ID: {id}, ErrorMessage: {e.Message}, InnerException: {e.InnerException}");
                throw;
            }
        }
        /// <summary>
        /// This will get all the survey results for a particular employee
        /// </summary>
        /// <param name="employeeId">EmployeeID</param>
        /// <returns></returns>
        public List<SurveyResult> GetEmployeeSurveyResults(string employeeId)
        {
            try
            {
                var results = _context.SurveyResults.Where(x => x.EmployeeId == employeeId).OrderByDescending(x=>x.EntryDt).ToList();
                return results;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: GetEmployeeSurveyResults :: ID: {employeeId}, Error: {e.Message}, InnerException: {e.InnerException}");
                throw;
            }
        }
        /// <summary>
        /// This gets the employee record for a particular employee
        /// </summary>
        /// <param name="employeeId">EmployeeID</param>
        /// <returns></returns>
        public Employee GetEmployee(string employeeId)
        {
            try
            {
                var employee = _context.Employees.FirstOrDefault(x => x.EmployeeId == employeeId);
                return employee;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: GetEmployee :: ID:  {employeeId}, Error: {e.Message}, InnerException: {e.InnerException}");
                throw;
            }
        }
    }
}
