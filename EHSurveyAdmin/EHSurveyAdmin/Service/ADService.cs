﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Security.Claims;
using System.Text;
using EHSurveyAdmin.Interfaces;
using Microsoft.AspNetCore.Http;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;

namespace EHSurveyAdmin.Service
{
    /// <summary>
    /// Class that handles all AD type of actions
    /// </summary>
    public class AdService : ILoggerInterface
    {
        private readonly ClaimsPrincipal _claim;
        private readonly IHttpContextAccessor _accessor;
        private readonly LoggerService _loggerService;
        public AdService(IHttpContextAccessor httpContextAccessor)
        {
            _accessor = httpContextAccessor;
            _claim = httpContextAccessor.HttpContext?.User;
            _loggerService = CreateLogger();
        }
        /// <summary>
        /// Returns the IPrincipal for the logged in user
        /// </summary>
        public IPrincipal CurrentUser
        {
            get
            {
                var context = _accessor.HttpContext;
                return context != null ? context.User : ClaimsPrincipal.Current;
            }
        }
        /// <summary>
        /// Determines if the logged in user is in the role specified
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>bool (yes/no) in role</returns>
        public bool IsInRole(string role)
        {
            var isInRole = false;
            var user = CurrentUser;
            if (user != null)
            {
                isInRole = user.IsInRole(role);
            }
            return isInRole;
        }
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("ADService");
            return logger;
        }
        /// <summary>
        /// Removes domain and slashes from username
        /// </summary>
        /// <param name="user">Logged in user</param>
        /// <returns></returns>
        public string CleanUsername(string user)
        {
            try
            {
                var findSlash = user.IndexOf("\\", StringComparison.Ordinal);
                user = user.Substring(findSlash + 1);
                return user.ToUpper();
            }
            catch (Exception e)
            {
                // log eventually
                return null;
            }
            
        }
        /// <summary>
        /// Gets logged in user
        /// </summary>
        /// <returns>username</returns>
        public string GetUserName()
        {
            try
            {
                return _accessor.HttpContext?.User.Identity?.Name;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetUserName occurred :: Details :: " + e.Message + " " + e.InnerException??"");
                throw;
            }
        }
        /// <summary>
        /// Get full name for user
        /// </summary>
        /// <param name="userId">logged in user's user id</param>
        /// <returns></returns>
        public string GetFullName(string userId)
        {
            try
            {
                var filter = new StringBuilder();
                filter.Append($"(sAMAccountName={userId})");
                var search = new DirectorySearcher(new DirectoryEntry())
                {
                    PageSize = 1000,
                    Filter = $"(&(objectClass=user)(objectCategory=person)(|{filter}))"
                };
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("givenName");

                var result = search.FindOne();
                var managerName = GetProperty(result, "displayname");
                return managerName;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: IsUserPermitted :: UserID: " + userId + " occurred :: Details :: " + e.Message + " " + e.InnerException??"");
                throw;
            }
            
        }
        /// <summary>
        /// This pulls out the actual value from the AD property passed in
        /// </summary>
        /// <param name="searchResult">Search Result passed in that contains the properties specified</param>
        /// <param name="propertyName">Property searching</param>
        /// <returns></returns>
        public string GetProperty(SearchResult searchResult, string propertyName)
        {
            try
            {
                return searchResult.Properties.Contains(propertyName)
                    ? searchResult.Properties[propertyName][0].ToString()
                    : string.Empty;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetProperty occurred :: Details :: " + e.Message + " " +
                    e.InnerException ?? "");
                throw;
            }
        }
    }
}