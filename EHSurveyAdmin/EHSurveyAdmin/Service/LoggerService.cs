﻿using NLog;
using System;

namespace EHSurveyAdmin.Service
{
    /// <summary>
    /// This is the Logger Service that is used through this application to log errors and/or information
    /// </summary>
    public class LoggerService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor used to construct and instance of the logger
        /// </summary>
        /// <param name="fileName">The name of the Log to be created</param>
        public LoggerService(string fileName)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var datetime = DateTime.Now.Date.ToString("MM-dd-yy");
            var logFileName = @"Logs\" + fileName + datetime + ".log";
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = logFileName};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }
        /// <summary>
        /// Writes Information to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteInfo(string text)
        {
            Logger.Info(text);
        }
        /// <summary>
        /// Writes Error to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteError(string text)
        {
            Logger.Error(text);
        }
    }
}
